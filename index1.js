var express = require('express')
var bodyparser = require('body-parser')
var cors = require('cors');

var app = express();

var dis = require('./control/display')
//var reg = require('./controllers/reg_control')
//var des = require('./controllers/destroy')
//var upd = require('./controllers/update')
//var userController = require('./controllers/userInfoController')

app.use(bodyparser.urlencoded({
    extended: true
}))
app.use(bodyparser.json())
app.use(cors())
// ---------------------- Serving front-end  ------------------ //

app.use(express.static(__dirname+'/front-end'));

//  console.log('hey')
app.post('/api/display', dis.display);
//app.get('/api/getuser', userController.getUserInfo)
//app.post('/api/authenticate', authen.authenticate);
//app.post('/api/delete', des.delete);
//app.post('/api/updat', upd.updat);
// console.log('hello')
//app.post('/controllers/authen', authen.authenticate);
//app.post('/controllers/register', reg.register);
//app.post('/controllers/updat', upd.updat);
// console.log('hello')


app.listen(8012, function (err, data) {
    if (err)
        console.log('err', err)
    else
        console.log('App started at port 8012');
});